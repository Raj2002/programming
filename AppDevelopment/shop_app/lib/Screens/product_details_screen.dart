import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/provider/product_template.dart';
import 'package:shop_app/provider/product.dart';

class Product_Details_Screen extends StatelessWidget {
  static const routeName = '/Product_Details_Screen';

  @override
  Widget build(BuildContext context) {
    dynamic receivedObject = ModalRoute.of(context)?.settings.arguments;

    Product? _singleProductItem = Provider.of<Products>(context, listen: false)
        .items
        .firstWhere((element) => element.id == receivedObject['Id']);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          _singleProductItem.title,
          style: const TextStyle(
            fontFamily: 'Manrope',
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.topCenter,
              width: double.infinity,
              height: 300,
              padding: const EdgeInsets.only(top: 30),
              child: Image.network(_singleProductItem.imageUrl),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                '₹${_singleProductItem.price.toString()}',
                style: const TextStyle(
                  fontSize: 20,
                  fontFamily: 'Manrope',
                  fontWeight: FontWeight.w400,
                  color: Colors.black54,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  _singleProductItem.description,
                  style: const TextStyle(
                    fontSize: 22,
                    fontFamily: 'Manrope',
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
