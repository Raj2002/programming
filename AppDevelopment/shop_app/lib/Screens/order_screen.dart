import 'package:flutter/material.dart';
import 'package:shop_app/Screens/AppDrawer.dart';
import 'package:shop_app/widgets/orderItemWidget.dart';
import '../provider/order.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  static const routeName = '/OrderScreen';

  @override
  State<OrderScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  bool isInit = true;
  bool loadingSpinner = false;
  @override
  void didChangeDependencies() async {
    if (isInit) {
      setState(() {
        loadingSpinner = true;
      });
      try {
        await Provider.of<Order>(context, listen: false).fetchOrderedItems();
      } catch (error) {
        //...
      }
      setState(() {
        loadingSpinner = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final order = Provider.of<Order>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Orders'),
      ),
      drawer: AppDrawer(),
      body: loadingSpinner
          ? const Center(child: CircularProgressIndicator())
          : RefreshIndicator(
              onRefresh: () async {
                try {
                  await Provider.of<Order>(context, listen: false)
                      .fetchOrderedItems();
                } catch (error) {
                  //...
                }
              },
              child: ListView.builder(
                itemBuilder: (ctx, index) =>
                    OrderItemWidget(order.OrderItems[index]),
                itemCount: order.OrderItems.length,
              ),
            ),
    );
  }
}
