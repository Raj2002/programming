import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/Screens/Edit_Screen.dart';
import 'package:shop_app/Screens/User_product_Screen.dart';
import 'package:shop_app/Screens/cart_screen.dart';
import 'package:shop_app/Screens/order_screen.dart';
import 'package:shop_app/Screens/product_details_screen.dart';
import 'package:shop_app/provider/cart.dart';
import 'package:shop_app/provider/order.dart';
import 'Screens/product_overview_screen.dart';
import 'provider/product.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  static const TextStyle fonttheme = TextStyle(
    fontFamily: 'Manrope',
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Products()),
        ChangeNotifierProvider(create: (context) => Cart()),
        ChangeNotifierProvider(create: (context) => Order()),
      ],
      child: MaterialApp(
        title: 'Shopii',
        theme: ThemeData(
            //primaryColor: Color.fromARGB(255, 248, 139, 184),
            colorScheme: ColorScheme.fromSeed(
                seedColor: Color.fromARGB(255, 255, 225, 245))),
        routes: {
          '/': (context) => Pruduct_overview_screen(),
          Product_Details_Screen.routeName: (context) =>
          Product_Details_Screen(),
          CartScreen.routeName: (context) => CartScreen(),
          OrderScreen.routeName: (context) => OrderScreen(),
          UserProductScreen.routeName: (context) => UserProductScreen(),
          EditScreen.routeName: (context) => EditScreen(),
        },
      ),
    );
  }
}
