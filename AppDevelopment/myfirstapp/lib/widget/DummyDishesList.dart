import 'package:flutter/material.dart';
import '../model/Category.dart';
const Dummy_Categories = const[
  Category(
    id: 'E1',
    title: 'Italien',
    color: Colors.amber ),

  Category(
    id: 'E2',
    title: 'Hamburgers',
    color: Colors.red ),
  
  Category(
    id: 'E3',
    title: 'French',
    color: Colors.green ),
  
  Category(
    id: 'E4',
    title: 'Summer',
    color: Colors.lime ),
  
  Category(
    id: 'E5',
    title: 'Asian',
    color: Colors.pink ),
];
