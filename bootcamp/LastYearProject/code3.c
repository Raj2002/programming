//https://www.google.com/maps/44.277549,-78.338084
//sprintf(url, "https://www.google.com/maps/search/?api=1&query=%.6f,%.6f", latitude, longitude);
#include <stdio.h>
#include <math.h>

typedef struct Location{
    double latitude;
    double longitude;
}location;
double CalculateDistance(location myLocation, location DestPetrolpump){
        
        double lat1Deg = myLocation.latitude * (3.14 / 180.0);
        double log1Deg = myLocation.longitude * (3.14 / 180.0);
        
        double lat2Deg = DestPetrolpump.latitude * (3.14 / 180.0);
        double log2Deg = DestPetrolpump.longitude * (3.14 / 180.0);
        
        double Difflat = lat2Deg - lat1Deg;
        double Difflog = log1Deg - log2Deg;
        
        double a = sin(Difflat/2) * sin(Difflog/2) + cos(lat1Deg) * cos(lat2Deg) * sin(Difflog/2) * sin(Difflog/2);
        
        double c = 2 * atan2(sqrt(a),sqrt(1-a));
        
        double distance = 6378 * c;
        
        return distance;
}
void main(){
    
    location MyLocation;
    MyLocation.latitude = 18.455111;
    MyLocation.longitude = 73.820602;
    
    location PetrolPumps[] = {
      {44.878757 , -67.989545},
      {98.983923 , -23.302903},
      {65.656478 , -87.887799},
      {87.878789 , -67.982978},
    };
    
    int pumpCount = sizeof(PetrolPumps) / sizeof(PetrolPumps[0]);
    printf("count  = %d\n",pumpCount);
    
    double minDistance = 17868;
    int indexOfNearestPump = -1;
    
    for(int i=0;i<pumpCount;i++){
        
            double distance = CalculateDistance(MyLocation,PetrolPumps[i]);   
            if(distance < minDistance){
                minDistance = distance;
                indexOfNearestPump = i;
            }
    }
    
    printf("latitude = %lf,longitude = %lf\n",PetrolPumps[indexOfNearestPump].latitude,PetrolPumps[indexOfNearestPump].longitude);
}
