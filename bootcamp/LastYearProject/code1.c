#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>
#include <json-c/json.h>

// Callback function to receive API response
size_t write_callback(void *contents, size_t size, size_t nmemb, char **output)
{
    size_t realsize = size * nmemb;
    *output = realloc(*output, realsize + 1);
    memcpy(*output, contents, realsize);
    (*output)[realsize] = '\0';
    return realsize;
}

int main()
{
    CURL *curl;
    CURLcode res;

    // Prepare the API request URL
    const char *api_url = "https://maps.googleapis.com/maps/api/geocode/json?address=PETROLEUM_PUMP_ADDRESS&key=YOUR_API_KEY";
    // Replace "PETROLEUM_PUMP_ADDRESS" with the actual address of the petroleum pump
    // Replace "YOUR_API_KEY" with your Google Maps Geocoding API key

    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl = curl_easy_init();
    if (curl)
    {
        // Set the URL
        curl_easy_setopt(curl, CURLOPT_URL, api_url);

        // Set the callback function to receive the response
        char *response = NULL;
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

        // Perform the request
        res = curl_easy_perform(curl);
        if (res == CURLE_OK)
        {
            // Parse the JSON response
            struct json_object *json, *results, *location;
            json = json_tokener_parse(response);
            json_object_object_get_ex(json, "results", &results);
            json_object *result = json_object_array_get_idx(results, 0);
            json_object_object_get_ex(result, "geometry", &location);
            json_object *geometry = json_object_object_get(location, "location");
            double latitude = json_object_get_double(json_object_object_get(geometry, "lat"));
            double longitude = json_object_get_double(json_object_object_get(geometry, "lng"));

            // Print the latitude and longitude
            printf("Latitude: %f\n", latitude);
            printf("Longitude: %f\n", longitude);
        }
        else
        {
            printf("Request failed: %s\n", curl_easy_strerror(res));
        }

        // Clean up
        curl_easy_cleanup(curl);
        curl_global_cleanup();

        // Free allocated memory
        free(response);
    }

    return 0;
}
