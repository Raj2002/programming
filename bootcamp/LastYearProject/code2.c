#include <math.h>
#include <stdio.h>
#include <stdlib.h>
// Structure to hold latitude and longitude
typedef struct {
    double latitude;
    double longitude;
} Coordinate;

// Function to calculate distance between two coordinates using the Haversine formula
double calculate_distance(Coordinate coord1, Coordinate coord2)
{
    double lat1_rad = coord1.latitude * (3.14 / 180.0);
    double lon1_rad = coord1.longitude * (3.14 / 180.0);
    double lat2_rad = coord2.latitude * (3.14 / 180.0);
    double lon2_rad = coord2.longitude * (3.14 / 180.0);

    double dlon = lon2_rad - lon1_rad;
    double dlat = lat2_rad - lat1_rad;

    double a = sin(dlat / 2) * sin(dlat / 2) + cos(lat1_rad) * cos(lat2_rad) * sin(dlon / 2) * sin(dlon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));

    double distance = 6378.1 * c;
    return distance;
}

int main()
{
    // Your current location
    Coordinate myLocation;
    myLocation.latitude = 40.123456;
    myLocation.longitude = -75.654321;

    // Database of petroleum pump locations
    Coordinate petroleumPumps[] = {
        {39.987654, -75.987654},  // Pump 1
        {39.876543, -75.876543},  // Pump 2
        {40.012345, -75.012345}   // Pump 3
        // Add more pump locations as needed
    };

    // Find the nearest petroleum pump
    int numPumps = sizeof(petroleumPumps) / sizeof(petroleumPumps[0]);
    double minDistance = 1984984; //random number
    int nearestPumpIndex = -1;

    for (int i = 0; i < numPumps; i++) {
        double distance = calculate_distance(myLocation, petroleumPumps[i]);
        if (distance < minDistance) {
            minDistance = distance;
            nearestPumpIndex = i;
        }
    }

    // Get the coordinates of the nearest pump
    Coordinate nearestPump = petroleumPumps[nearestPumpIndex];

    // Use the nearest pump coordinates as needed

    while (1) {
        // Your main program loop
    }

    return 0;
}

